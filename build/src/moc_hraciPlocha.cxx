/****************************************************************************
** Meta object code from reading C++ file 'hraciPlocha.h'
**
** Created: Fri Jun 11 15:04:16 2010
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/hraciPlocha.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'hraciPlocha.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HraciPlocha[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      27,   12,   12,   12, 0x05,
      39,   12,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      48,   12,   12,   12, 0x0a,
      60,   12,   12,   12, 0x0a,
      70,   12,   12,   12, 0x0a,
      83,   12,   12,   12, 0x0a,
      93,   12,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HraciPlocha[] = {
    "HraciPlocha\0\0sebranBonus()\0novyLevel()\0"
    "Prohra()\0novyBonus()\0novyTah()\0"
    "ukoncitHru()\0uvolnit()\0blokovat()\0"
};

const QMetaObject HraciPlocha::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_HraciPlocha,
      qt_meta_data_HraciPlocha, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HraciPlocha::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HraciPlocha::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HraciPlocha::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HraciPlocha))
        return static_cast<void*>(const_cast< HraciPlocha*>(this));
    return QWidget::qt_metacast(_clname);
}

int HraciPlocha::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sebranBonus(); break;
        case 1: novyLevel(); break;
        case 2: Prohra(); break;
        case 3: novyBonus(); break;
        case 4: novyTah(); break;
        case 5: ukoncitHru(); break;
        case 6: uvolnit(); break;
        case 7: blokovat(); break;
        default: ;
        }
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void HraciPlocha::sebranBonus()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void HraciPlocha::novyLevel()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void HraciPlocha::Prohra()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE
