#ifndef HRACIPLOCHA_H
#define HRACIPLOCHA_H

#include <QWidget>

#include "had.h"

class QTimer;

class HraciPlocha : public QWidget
{
	Q_OBJECT
	
public:
	HraciPlocha(int novaMrizka, int nasobekSirky, int nasobekVysky, QWidget * parent = 0);
	
	void ovladani(QKeyEvent *event);
	
public slots:
	void novyBonus();
	void novyTah();
	void ukoncitHru();

	void uvolnit()
	{
		if(pristiSmer == 0)
		{
			stisknuto = false;
		}
	}
	void blokovat() { stisknuto = true; }
	
signals:
	void sebranBonus();
	void novyLevel();
	void Prohra();
	
protected:
	void paintEvent(QPaintEvent *event);
	
private:
	int	mrizka;
	
	Had *had1;
	Had *had2;
	Clanek *bonus;
	
	QTimer *timer;

	int pristiSmer;
	
	bool stisknuto;
	bool konecHry;
};

#endif
