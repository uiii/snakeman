#ifndef HLAVNIOKNO_H
#define HLAVNIOKNO_H

#include <QWidget>
#include <QKeyEvent>

#include "hraciPlocha.h"
#include "pocitadlo.h"

class HlavniOkno : public QWidget
{
public:
	HlavniOkno(const QString &title, QWidget *parent = 0);
	
protected:	
	void keyPressEvent(QKeyEvent *event);
	
private:
	HraciPlocha *hraciPlocha;
	Pocitadlo *level;
	Pocitadlo *body;
};

#endif
