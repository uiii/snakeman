#ifndef HAD_H
#define HAD_H

#include <QWidget>
#include <QPainter>

enum Typ {TELO, BONUS};

// CLANEK
class Clanek
{
public:
	Clanek(Typ tp, Clanek *pred, int x = 0, int y = 0)
		:predchozi(pred),souradnice_x(x),souradnice_y(y),typ(tp){}
		
	int x(){ return souradnice_x; }
	int y(){ return souradnice_y; }
	Clanek * pred(){ return predchozi; }
	
	void nastav_xy(int x, int y){ souradnice_x = x; souradnice_y = y; }
	void nastav_pred(Clanek *pred){ predchozi = pred; }
	
	void vykreslit(QPainter &painter, int polomer);
	
private:
	Clanek *predchozi;
	int souradnice_x;
	int souradnice_y;
	
	Typ typ;
};

// HAD
class Had : public QWidget
{
	Q_OBJECT
	
public:
	Had(int r, QWidget * parent = 0);
	
	int ziskej_polomer(){ return polomer; }
	int ziskej_delku(){ return delka; }
	
	Clanek * ziskej_zacatek(){ return zacatek; }

	void vykreslit(QPainter &painter);
	
public slots:
	void pohyb();
	void pridat();
	void zmenit_smer(int smer);
	
signals:
	void dopohybovano();
	void sebranBonus();
		
private:
	int delka; // pocet clanku hada
	int polomer; // polomer jednoho clanku
	
	int smer_x;
	int smer_y;
	
	Clanek *zacatek;
	Clanek *konec;
};

#endif
