#include <QLCDNumber>
#include <QLabel>
#include <QVBoxLayout>

#include "pocitadlo.h"

Pocitadlo::Pocitadlo(const QString &text, int width, int height, int hodnota, int rad, QWidget *parent)
	:QWidget(parent)
{
	cislo = new QLCDNumber(rad);
	cislo->display(hodnota);
	cislo->setSegmentStyle(QLCDNumber::Filled);
	
	popisek = new QLabel;
	popisek->setText(text);
	popisek->setFont(QFont("Arial", 14, QFont::Bold));
	popisek->setAlignment(Qt::AlignHCenter | Qt::AlignTop);
	
	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(popisek);
	vbox->addWidget(cislo);
	vbox->setStretchFactor(cislo, 2);
	
	setLayout(vbox);
	
	setFixedSize(width, height);
}

void Pocitadlo::zvysit(int plus)
{
	cislo->display(cislo->value()+plus);
}

void Pocitadlo::snizit(int minus)
{
	cislo->display(cislo->value()-minus);
}
