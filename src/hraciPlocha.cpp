#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QKeyEvent>
#include <QTextStream>

#include "had.h"
#include "hraciPlocha.h"

HraciPlocha::HraciPlocha(int novaMrizka, int nasobekSirky, int nasobekVysky, QWidget * parent)
	:QWidget(parent)
{
	// Vlastnosti
	mrizka = novaMrizka;
	pristiSmer = 0;
	stisknuto = false;
	konecHry = false;
	
	had1 = new Had(mrizka);
	had2 = new Had(mrizka);
	bonus = new Clanek(BONUS, 0);
		
	timer = new QTimer;
	
	// Nastaveni
	setFixedSize(nasobekSirky * mrizka, nasobekVysky * mrizka);
	
	setPalette(QPalette(QColor(255, 255, 255)));
	setAutoFillBackground(true);
	
	QTime time(0, 0, 0);
	qsrand(time.secsTo(QTime::currentTime())); // kvuli nahodnemu generovani
	
	connect(timer, SIGNAL(timeout()), this, SLOT(uvolnit()));
	connect(timer, SIGNAL(timeout()), this, SLOT(novyTah()));
	
	novyBonus();
	
	timer->start(300);
}

void HraciPlocha::novyBonus()
{
	while(1)
	{
		int x = mrizka + ((qrand() % (width() / (2*mrizka))) * (2*mrizka));
		int y = mrizka + ((qrand() % (height() / (2*mrizka))) * (2*mrizka));
		
		QPixmap pixmap = QPixmap::grabWidget(this);
		QImage image = pixmap.toImage();
		QRgb color = image.pixel(x, y);
		QColor bila(Qt::white);
		
		if(color == bila.rgb())
		{
			bonus->nastav_xy(x, y);
			break;
		}
	}
}

void HraciPlocha::novyTah()
{
	QPixmap pixmap = QPixmap::grabWidget(this);
	QImage image = pixmap.toImage();
	
	had->pohyb();
	
	Clanek *zacatek = had->ziskej_zacatek();
	int x = zacatek->x();
	int y = zacatek->y();
	
	if(x == bonus->x() && y == bonus->y())
	{
		emit sebranBonus();
		
		timer->setInterval(timer->interval()-3);
		if((timer->interval() % 30) == 0) emit novyLevel();
		
		had->pridat();
		
		novyBonus();
	}
	
	if(x < 0 || x > width() || y < 0 || y > height())
	{
		ukoncitHru();
	}
	else
	{
		QRgb color = image.pixel(x, y);
		QColor modra(Qt::blue);
		if(color == modra.rgb())
		{
			ukoncitHru();
		}
		else
		{
			update();
		}
	}

	if(pristiSmer != 0)
	{
		had->zmenit_smer(pristiSmer);
		pristiSmer = 0;
	}
}

void HraciPlocha::ukoncitHru()
{
	timer->stop();
	konecHry = true;
	update();
}

void HraciPlocha::paintEvent(QPaintEvent * /* event */)
{
	QPainter painter(this);
	
	painter.setRenderHints(QPainter::Antialiasing, true);
	
	painter.setPen(Qt::NoPen);
    
    // VYKRESLIT HADA
	had->vykreslit(painter);
	
	// VYKRESLIT BONUS
	bonus->vykreslit(painter, mrizka);
	
	if(konecHry)
	{
		painter.setFont(QFont("Times New Roman", 40, QFont::Bold));
		painter.setPen(Qt::red);
		painter.drawText(QRect(0, height()/2-30, width(), 60), Qt::AlignCenter, tr("Konec hry!"));
	}
}

void HraciPlocha::ovladani(QKeyEvent *event)
{
	int smer = 0;

	switch(event->key())
	{
		case Qt::Key_Left:
			smer = 1;
		break;
		case Qt::Key_Right:
			smer = 2;
		break;
		case Qt::Key_Up:
			smer = 3;
		break;
		case Qt::Key_Down:
			smer = 4;
		break;
		default:
			smer = 0;
		break;
	}

	if(smer != 0)
	{
		if(stisknuto)
		{
			if(pristiSmer == 0)
			{
				pristiSmer = smer;
			}
		}
		else
		{
			had->zmenit_smer(smer);
			blokovat();
		}
	}

	/*if(!stisknuto)
	{
		switch(event->key())
		{
			case Qt::Key_Left:
				had->zmenit_smer(1);
				blokovat();
			break;
			case Qt::Key_Right:
				had->zmenit_smer(2);
				blokovat();
			break;
			case Qt::Key_Up:
				had->zmenit_smer(3);
				blokovat();
			break;
			case Qt::Key_Down:
				had->zmenit_smer(4);
				blokovat();
			break;
			default:
			break;
		}
	}*/
}
