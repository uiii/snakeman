#ifndef POCITADLO_H
#define POCITADLO_H

#include <QWidget>

class QLCDNumber;
class QLabel;

class Pocitadlo : public QWidget
{
	Q_OBJECT
	
public:
	Pocitadlo(const QString &text, int width, int height, int hodnota = 0, int rad = 3, QWidget *parent = 0);
	
public slots:
	void zvysit(int plus = 1);
	void snizit(int minus = 1);
	
private:
	QLCDNumber *cislo;
	QLabel *popisek;
};

#endif
