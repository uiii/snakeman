#include <QApplication>

#include "hlavniOkno.h"

int main(int argc, char* argv[])
{
	QApplication had(argc, argv);
	
	HlavniOkno hlavniOkno("Snake - Man 1.0");
	hlavniOkno.show();
	
	return had.exec();	
}
