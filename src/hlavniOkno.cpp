#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "hlavniOkno.h"
#include "hraciPlocha.h"
#include "pocitadlo.h"

HlavniOkno::HlavniOkno(const QString &title, QWidget * parent)
	:QWidget(parent, Qt::Window)
{
	hraciPlocha = new HraciPlocha(6, 50, 50);
	
	level = new Pocitadlo(tr("Level"), 100, 100, 1);
	body = new Pocitadlo(tr("Body"), 100, 100);
	
	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(level);
	hbox->addWidget(body);
	
	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addLayout(hbox);
	vbox->addWidget(hraciPlocha);
	
	connect(hraciPlocha, SIGNAL(sebranBonus()), body, SLOT(zvysit()));
	connect(hraciPlocha, SIGNAL(novyLevel()), level, SLOT(zvysit()));
	
	setFixedSize(sizeHint());
	setWindowTitle(title);
	setLayout(vbox);
}

void HlavniOkno::keyPressEvent(QKeyEvent *event)
{
	switch(event->key())
	{
		case Qt::Key_Escape:
			qApp->quit();
		break;
		
		default:
			hraciPlocha->ovladani(event);
		break;
	}
}
