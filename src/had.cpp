#include <QPainter>
#include <QTextStream>

#include "had.h"

// CLANEK
void Clanek::vykreslit(QPainter &painter, int polomer)
{
	if(souradnice_x && souradnice_y)
	{
		switch(typ)
		{
			case TELO:
				painter.setBrush(Qt::blue);
				painter.drawEllipse(souradnice_x-polomer, souradnice_y-polomer, 2*polomer, 2*polomer);
			break;
			case BONUS:
				painter.setBrush(Qt::red);
				painter.drawEllipse(souradnice_x-polomer, souradnice_y-polomer, 2*polomer, 2*polomer);
			break;
		}
	}
}

// HAD
Had::Had(int mrizka, QWidget * parent)
	:QWidget(parent)
{
	delka = 3;
	polomer = mrizka;
	
	zacatek = new Clanek(TELO, 0, 5*polomer, polomer);
	konec = new Clanek(TELO, zacatek, 3*polomer, polomer);
	konec = new Clanek(TELO, konec, polomer, polomer);
	
	// smer doprava
	smer_x = 1;
	smer_y = 0;
}

void Had::pridat()
{
	konec = new Clanek(TELO, konec);
	delka++;
}

void Had::pohyb()
{	
	zacatek->nastav_pred(konec);
	konec = konec->pred();
	
	zacatek->pred()->nastav_xy(zacatek->x()+smer_x*2*polomer, zacatek->y()+smer_y*2*polomer);
	
	zacatek = zacatek->pred();
	zacatek->nastav_pred(0);
	
	emit dopohybovano();
}

void Had::zmenit_smer(int smer)
{
	switch(smer)
	{
		case 1: // doleva
			if(smer_x == 1 && smer_y == 0) break;
			smer_x = -1;
			smer_y = 0;
		break;
		case 2: // doprava
			if(smer_x == -1 && smer_y == 0) break;
			smer_x = 1;
			smer_y = 0;
		break;
		case 3: // nahoru
			if(smer_x == 0 && smer_y == 1) break;
			smer_x = 0;
			smer_y = -1;
		break;
		case 4: // dolu
			if(smer_x == 0 && smer_y == -1) break;
			smer_x = 0;
			smer_y = 1;
		break;
	}
}

void Had::vykreslit(QPainter &painter)
{	
    Clanek *akt = konec;
    
    for(int i=0; i<delka; i++)
    {
    	akt->vykreslit(painter, polomer);
    	akt = akt->pred();
    }
}
